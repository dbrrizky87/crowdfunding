<?php

// Route::group([
//     'middleware'    => 'api',
//     'prefix'        => 'auth',
//     'namespace'     =>'Auth'

//     ], function (){
        
//     Route::post('register', 'RegisterController');
//     Route::post('login', 'LoginController');
//     Route::post('logout', 'LogoutController');

//     });

Route::namespace('Auth')->group(function () {

    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
    Route::post('verification', 'VerificationController');
    Route::post('regenerate-otp', 'RegenerateOtpCodeController');
    Route::post('update-password', 'UpdatePasswordCodeController');
});


Route::get('user', 'UserController');