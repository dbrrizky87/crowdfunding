<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;

class Otp extends Model
{
    use UsesUuid;
    protected $guarded = [];

    protected $table = "otp_codes";

    protected $fillable = [
        'user_id','otp', 'valid_until'
    ];


    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

}
