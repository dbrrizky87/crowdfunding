<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Otp;
use Carbon\Carbon;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        
        $request->validate([
            'otp' => 'required',
        ]);

         $otp_code = Otp::where ('otp' , $request->otp)->first();
         
         if (!$otp_code) {
            return response()->json([
              
                'response_code' => '01',
                'response_message' => 'Kode Otp tidak ditemukan '
            ], 401);
         }

         $now = Carbon::now();

         if ($now > $otp_code->valid_until){
            return response()->json([
              
                'response_code' => '01',
                'response_message' => 'Kode Otp sudah tidak berlaku, silahkan generate ulang!!'
            ], 401);
         }


        //  update user
        $user = User::find($otp_code->user_id);
        $user->email_verified_at = Carbon::now();
        $user->save();

        // delete otp
        $otp_code->delete();

        $data['user'] = $user;

        return response()->json([
              
            'response_code' => '00',
            'response_message' => 'User berhasil di verifikasi', 
            'data' => $data
        ], 200);
    }
}
