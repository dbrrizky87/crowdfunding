<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\UserOtpMail;
use App\User;
use App\Otp;
use Mail;

class RegenerateOtpCodeController extends Controller
{

    

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'email' => 'required',
        ]);

        $user_email = User::where ('email' , $request->email)->first();
        $otp = Otp::where('user_id',$user_email->id)->first();
        if (!$user_email) {
            
            return response()->json([
              
                'response_code' => '01',
                'response_message' => 'Email tidak ditemukan. Silahkan masukan email dengan benar'
            ], 401);

        } else {
        $user_email->generate_otp_code();
        Mail::to($user_email)->send(new UserOtpMail($otp));
        return response()->json([
              
            'response_code' => '00',
            'response_message' => 'Otp berhasil di generate ulang. Silahkan cek email Anda!! '
        ], 200);
        

        }
    }        
}