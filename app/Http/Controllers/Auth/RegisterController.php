<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Auth\RegisterRequest;
use App\Events\UserRegisteredEvent;
use App\User;
use App\Otp;
use Carbon\Carbon;


class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        
       $user = User::create([
            'name'      => $request['name'],
            'email'     => $request ['email'],
            'phone'     => $request ['phone'],
            'password'  => bcrypt($request['password'])
        ]);
       
       
        // $data_request = $request->all();
        // $user = User::create($data_request);
        $data['user'] = $user;

        $user = User::where ('email',$request->email)->first();
        $otp = Otp::where('user_id',$user->id)->first();

        event(new UserRegisteredEvent($user));

        return response()->json([
              
            'response_code' => '00',
            'response_message' => 'User baru berhasil didaftarkan, silahkan cek email untuk melihat kode otp', 
            'data' => $data
        ], 200);

    }
}
